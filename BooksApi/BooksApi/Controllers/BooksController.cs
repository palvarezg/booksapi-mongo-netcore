﻿using System.Collections.Generic;
using BooksApi.Models;
using BooksApi.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookPersistence _bookPersistence;

        public BooksController(BookPersistence bookPersistence)
        {
            _bookPersistence = bookPersistence;
        }

        [HttpGet]
        public ActionResult<List<Book>> Get() =>
            _bookPersistence.Get();

        [HttpGet("{id:length(24)}", Name = "GetBook")]
        public ActionResult<Book> Get(string id)
        {
            var book = _bookPersistence.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            return book;
        }

        [HttpPost]
        public ActionResult<Book> Create(Book book)
        {
            _bookPersistence.Create(book);

            return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Book bookIn)
        {
            var book = _bookPersistence.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookPersistence.Update(id, bookIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var book = _bookPersistence.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookPersistence.Remove(book.Id);

            return NoContent();
        }
    }
}
